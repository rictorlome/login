require 'selenium-webdriver'
require 'bcrypt'
user = "rictorlome"
pass = BCrypt::Password.new("$2a$10$1KWamm0Otk6R0CG9PGp1huBMNRwIg0tf.kyG4DXdMy2M3n2mcCw7a")

driver = Selenium::WebDriver.for :chrome
driver.navigate.to "https://progress.appacademy.io"

wait = Selenium::WebDriver::Wait.new(:timeout => 10)

element = driver.find_element(link_text: 'Sign in with Github!')
element.click

wait = Selenium::WebDriver::Wait.new(:timeout => 10)

element = driver.find_element(id: 'login_field')
element.send_keys user

element = driver.find_element(id: 'password')
element.send_keys pass

element = driver.find_element(name: 'commit')
element.click

wait = Selenium::WebDriver::Wait.new(:timeout => 10)

if Time.now.hour.between?(8,10)
    element = driver.find_element(xpath: '/html/body/main/section/ul/li[1]/form/button')
    element.click
elsif Time.now.hour.between?(12,14)
    element = driver.find_element(xpath: '/html/body/main/section/ul/li[2]/form/button')
    element.click
elsif Time.now.hour.between?(15,17)
    element = driver.find_element(xpath: '/html/body/main/section/ul/li[3]/form/button')
    element.click
end    
